using lab.framework;
using UnityEngine;

namespace ao
{
    public class AppBootstrap : BootstrapBase
    {
        [SerializeField] private InitSettings initSettings;

        private AppEventsSystem appEventsSystem;
        
        // networking
        private RequestsSystem requestsSystem;
        private FetchProgramsNetworkSystem fetchProgramsNetworkSystem;
        private ProgramImageSystem programImageSystem;
        
        // ui
        private UIScrollViewSystem uiScrollViewSystem;
        private UISearchSystem uiSearchSystem;
        private UIAdditionItemInfoSystem uiAdditionItemInfoSystem;
        private UIStatesSystem uiStatesSystem;
        
        // data
        private IParseDataSystem parseDataSystem;
       
        protected override void Awake()
        {
            base.Awake();
            
            Application.targetFrameRate = 60;

            initSettings.AsyncProcessor = CreateAsyncProcessor();

            appEventsSystem = systemsContainer.Register(typeof(AppEventsSystem));
            
            // network
            requestsSystem = systemsContainer.Register(typeof(RequestsSystem));
            fetchProgramsNetworkSystem = systemsContainer.Register(typeof(FetchProgramsNetworkSystem));
            programImageSystem = systemsContainer.Register(typeof(ProgramImageSystem));
            
            // ui
            uiScrollViewSystem = systemsContainer.Register(typeof(UIScrollViewSystem));
            uiSearchSystem = systemsContainer.Register(typeof(UISearchSystem));
            uiAdditionItemInfoSystem = systemsContainer.Register(typeof(UIAdditionItemInfoSystem));
            uiStatesSystem = systemsContainer.Register(typeof(UIStatesSystem));

            // data
            parseDataSystem = systemsContainer.Register<IParseDataSystem>(typeof(ParseDataSystem));
            
            systemsContainer.ResolveAll();
            systemsContainer.InitializeAll(initSettings);
            systemsContainer.LateInitializeAll();
        }
    }
}