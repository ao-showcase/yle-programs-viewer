using System;

namespace ao
{
    [Serializable]
    public struct ProgramData
    {
        public string Title;
        public string ContentType;
        public string Description;
        public string Creator;
        public string Country;

        public ImageData imageData;
    }
}