using System;

namespace ao
{
    [Serializable]
    public struct ImageData
    {
        public string Id;
        public bool Available;
    }
}