using UnityEngine;
using UnityEngine.UI;

namespace ao
{
    [RequireComponent(typeof(Animator), typeof(Button))]
    public class AnimationBoolRunner : MonoBehaviour
    {
        [SerializeField]
        private string param = "Show";
        
        private Animator animator;
        private Button button;
        
        private void Awake()
        {
            animator = GetComponent<Animator>();
            button = GetComponent<Button>();

            if (!animator || !button)
                return;
            
            button.onClick.AddListener(() =>  animator.SetBool(param, !animator.GetBool(param)));
        }
    }
}