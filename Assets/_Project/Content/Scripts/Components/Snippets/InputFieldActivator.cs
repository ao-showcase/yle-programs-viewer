using UnityEngine;
using UnityEngine.UI;

namespace ao
{
    [RequireComponent(typeof(Button))]
    public class InputFieldActivator : MonoBehaviour
    {
        private Button button;
        private InputField inputField;
        
        private void Awake()
        {
            button = GetComponent<Button>();
            inputField = GetComponentInChildren<InputField>(true);

            if (!button)
                return;
            
            button.onClick.AddListener(ActivateInputField);
        }

        private void ActivateInputField()
        {
            inputField.Select();
            inputField.ActivateInputField();
        }
    }
}