using UnityEngine;
using UnityEngine.UI;

namespace ao
{
    public class UIScrollView : MonoBehaviour
    {
        public ScrollRect ScrollRect;
        
        private void Awake()
        {
            if (!ScrollRect)
            {
                ScrollRect = GetComponent<ScrollRect>();
            }
        }
    }
}