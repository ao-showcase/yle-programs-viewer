using UnityEngine;
using UnityEngine.UI;

namespace ao
{
    public class UIProgramInfo : MonoBehaviour
    {
        public Image Image;
        public Button CloseButton;
        public GameObject Loader;

        public Text Title;
        public Text Description;
        public Text ContentType;
        public Text Creator;
        public Text Country;
    }
}