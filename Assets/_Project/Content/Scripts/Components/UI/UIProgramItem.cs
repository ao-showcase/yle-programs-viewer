using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace ao
{
    public class UIProgramItem : MonoBehaviour
    {
        public Text Title;
        public ProgramData ProgramData;
        
        public UnityAction<ProgramData> OnItemClick;
        
        public void ItemClick()
        {
            OnItemClick?.Invoke(ProgramData);
        }
    }
}