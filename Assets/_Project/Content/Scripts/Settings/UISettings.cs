using UnityEngine;

namespace ao
{
    [CreateAssetMenu(fileName = "UISettings", menuName = "AO/UISettings")]
    public class UISettings : ScriptableObject
    {
        public UIProgramItem ItemPrefab;
    }
}