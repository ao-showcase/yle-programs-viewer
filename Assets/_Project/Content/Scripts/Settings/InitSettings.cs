using System;
using lab.framework;
using UnityEngine;

namespace ao
{
    [Serializable]
    public class InitSettings
    {
        public NetworkSettings NetworkSettings;
        public UISettings UISettings;
        
        [HideInInspector] 
        public AsyncProcessor AsyncProcessor;
    }
}