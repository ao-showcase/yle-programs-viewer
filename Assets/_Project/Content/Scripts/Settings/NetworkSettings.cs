using UnityEngine;

namespace ao
{
    [CreateAssetMenu(fileName = "NetworkSettings", menuName = "AO/NetworkSettings")]
    public class NetworkSettings : ScriptableObject
    {
        public int limit = 10;
        
        [HideInInspector]
        public string AppIdKey = "app_id=82c8f90b&app_key=d4c0ac2136f9d086d3f564a6758f7ee9";
    }
}