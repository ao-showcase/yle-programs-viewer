using System.Collections.Generic;
using lab.framework;
using SimpleJSON;

namespace ao
{
    public class ParseDataSystem : ISystem, IParseDataSystem
    {
        public List<ProgramData> Parse(string text)
        {
            List<ProgramData> programs = new List<ProgramData>();
            
            var jsonObject = JSON.Parse(text);

            if (jsonObject["data"].Count > 0)
            {
                foreach (var node in jsonObject["data"])
                {                    
                    ProgramData programData = new ProgramData();
                    programData.Title = SetTitle(node.Value);
                    programData.imageData = SetImageData(node.Value);
                    programData.Description = SetDescription(node.Value);
                    programData.ContentType = SetContentType(node.Value);
                    programData.Country = SetCountry(node.Value);
                    programData.Creator = SetCreator(node.Value);
                    
                    programs.Add(programData);
                }
            }

            return programs;
        }

        private string SetTitle(JSONNode node)
        {
            if (node["title"]["fi"])
            {
                return node["title"]["fi"];
            }
            
            if (!node["title"]["fi"] && node["title"]["und"])
            {
                return node["title"]["und"];
            }
            
            if (!node["title"]["fi"] && node["title"]["sv"])
            {
                return node["title"]["sv"] + " (sv)";
            }
            
            if (!node["title"]["fi"] && node["title"]["se"])
            {
                return node["title"]["se"] + " (se)";
            }

            return null;
        }

        private ImageData SetImageData(JSONNode node)
        {
            ImageData imageData = new ImageData();
            
            imageData.Available = node["image"]["available"];
                    
            if (imageData.Available)
            {
                imageData.Id = node["image"]["id"];
            }

            return imageData;
        }
        
        private string SetDescription(JSONNode node)
        {
            if (node["description"]["fi"])
            {
                return node["description"]["fi"];
            }
            
            if (!node["description"]["fi"] && node["description"]["und"])
            {
                return node["description"]["und"];
            }
            
            if (!node["description"]["fi"] && node["description"]["sv"])
            {
                return node["description"]["sv"] + "(sv)";
            }

            return null;
        }

        private string SetContentType(JSONNode node)
        {
            if (node["type"])
            {
                return node["type"];
            }

            return null;
        }

        private string SetCountry(JSONNode node)
        {
            JSONNode countryList = node["countryOfOrigin"];

            if (countryList[0])
            {
                return countryList[0];
            }
            return null;
        }

        private string SetCreator(JSONNode node)
        {
            JSONNode creatorList = node["creator"];
            string name = node["creator"][0]["name"];
            return name;
        }
        
    }
}