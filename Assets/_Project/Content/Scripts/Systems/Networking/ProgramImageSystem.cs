using System.Collections;
using lab.framework;
using UnityEngine;
using UnityEngine.Networking;

namespace ao
{
    public class ProgramImageSystem : BaseInitializableSystem
    {
        [Inject] private AppEventsSystem appEventsSystem;

        private string cdnUrl = "http://images.cdn.yle.fi/image/upload/w_850,h_850,c_fit/";
        private string format = ".jpg";
        
        public override void Initialize(params object[] arg)
        {
            base.Initialize(arg);
            
            appEventsSystem.OnItemClick += OnItemClick;
        }

        private void OnItemClick(ProgramData programData)
        {
            if (programData.imageData.Available)
            {
                RequestImage(programData.imageData);
            }
        }

        private void RequestImage(ImageData imageData)
        {
            var uri = string.Concat(cdnUrl,
                                    imageData.Id,
                                    format);
                                
            InitSettings.AsyncProcessor.StartCoroutine(GetTexture(uri, imageData.Id));
        }
        
        IEnumerator GetTexture(string uri, string id)
        {
            using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(uri))
            {
                yield return uwr.SendWebRequest();
    
                if (uwr.isNetworkError || uwr.isHttpError)
                {
                    Debug.Log(uwr.error);
                }
                else
                {
                    var texture = DownloadHandlerTexture.GetContent(uwr);
                    appEventsSystem.OnImageReceived?.Invoke(texture, id);
                }
            }
        }
    }
}