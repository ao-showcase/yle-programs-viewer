using System.Collections.Generic;

namespace ao
{
    public interface IParseDataSystem
    {
        List<ProgramData> Parse(string text);
    }
}