using System.Collections.Generic;
using lab.framework;

namespace ao
{
    public class FetchProgramsNetworkSystem : BaseInitializableSystem
    {
        [Inject] private AppEventsSystem appEventsSystem;
        [Inject] private RequestsSystem requestsSystem;
        [Inject] private IParseDataSystem parseDataSystem;
        
        private string fetchItemsUrl = "https://external.api.yle.fi/v1/programs/items.json?";
        private string offsetParam = "offset=";
        private string limitParam = "limit=";

        private int offset = 0;
        private string query;
        
        public override void Initialize(params object[] arg)
        {
            base.Initialize(arg);
           
            appEventsSystem.OnNewRequest += OnNewRequest;
            appEventsSystem.OnNextRequest += MakeRequest;
        }

        private void OnNewRequest(string searchQuery)
        {
            query = searchQuery;
            offset = 0;
            MakeRequest();
        }

        private void MakeRequest()
        {
            var uri = string.Concat(fetchItemsUrl, offsetParam, offset, '&', 
                                    limitParam, InitSettings.NetworkSettings.limit, '&', 
                                    "availability=ondemand&", 
                                    "type=program&", 
                                    "q=", query, '&', 
                                    "region=fi&", InitSettings.NetworkSettings.AppIdKey);
            
            requestsSystem.Request(uri, ParseRespond);

            offset += InitSettings.NetworkSettings.limit;
        }
        
        private void ParseRespond(string text)
        {
            List<ProgramData> programs = parseDataSystem.Parse(text);

            for (int i = 0; i < programs.Count; i++)
            {
                appEventsSystem.OnProgramDataReceived?.Invoke(programs[i], i == programs.Count - 1);
            }
        }
    }
}