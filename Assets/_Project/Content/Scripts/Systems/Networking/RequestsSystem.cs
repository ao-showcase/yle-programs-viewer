using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

namespace ao
{
    public class RequestsSystem : BaseInitializableSystem
    {
        public void Request(string uri, Action<string> OnDataFetch)
        {
            InitSettings.AsyncProcessor.StartCoroutine(GetRequest(uri, OnDataFetch));
        }

        IEnumerator GetRequest(string uri, Action<string> OnDataFetch)
        {
            using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
            {
                yield return webRequest.SendWebRequest();

                string[] pages = uri.Split('/');
                int page = pages.Length - 1;

                if (webRequest.isNetworkError)
                {
                    Debug.Log("Error: " + webRequest.error);
                }
                else
                {
                    OnDataFetch?.Invoke(webRequest.downloadHandler.text);
                }
            }
        }
    }
}