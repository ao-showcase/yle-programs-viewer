using lab.framework;
using UnityEngine;

namespace ao
{
    public class UISearchSystem : BaseInitializableSystem
    {
        [Inject] private AppEventsSystem appEventsSystem;
        
        private UISearchBar uiSearchBar;
        private string prevQuery = "";
       
        public override void Initialize(params object[] arg)
        {
            base.Initialize(arg);

            uiSearchBar = GameObject.FindObjectOfType<UISearchBar>();

            if (!uiSearchBar)
                return;
            
            uiSearchBar.SearchInput.onEndEdit.AddListener(OnClick);
        }

        private void OnClick(string text)
        {
            if (ValidRequest(text))
            {
                prevQuery = text;
                appEventsSystem.OnNewRequest?.Invoke(uiSearchBar.SearchInput.text);
            }
        }

        private bool ValidRequest(string text)
        {
            return text.Replace(" ", string.Empty).Length > 0 && !prevQuery.Equals(text);
        }
    }
}