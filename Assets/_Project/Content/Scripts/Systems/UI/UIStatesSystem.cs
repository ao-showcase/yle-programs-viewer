using lab.framework;
using UnityEngine;

namespace ao
{
    public class UIStatesSystem : BaseInitializableSystem
    {
        [Inject] private AppEventsSystem appEventsSystem;
        
        private UIStates uiStates;
        private static readonly int programViewParam = Animator.StringToHash("Program View");

        public override void Initialize(params object[] arg)
        {
            base.Initialize(arg);

            uiStates = GameObject.FindObjectOfType<UIStates>();
            
            appEventsSystem.OnItemClick += OnItemClick;
            appEventsSystem.OnItemClose += OnItemClose; 
        }

        private void OnItemClick(ProgramData programData)
        {
            uiStates.Animator.SetBool(programViewParam, true);
        }
        
        private void OnItemClose()
        {
            uiStates.Animator.SetBool(programViewParam, false);
        }
    }
}