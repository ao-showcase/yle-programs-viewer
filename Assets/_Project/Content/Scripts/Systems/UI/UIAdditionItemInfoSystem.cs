using lab.framework;
using UnityEngine;

namespace ao
{
    public class UIAdditionItemInfoSystem : BaseInitializableSystem
    {
        [Inject] private AppEventsSystem appEventsSystem;

        private UIProgramInfo uiProgramInfo;
        private string curImageId;
        
        public override void Initialize(params object[] arg)
        {
            base.Initialize(arg);

            uiProgramInfo = GameObject.FindObjectOfType<UIProgramInfo>();

            if (!uiProgramInfo)
                return;

            uiProgramInfo.CloseButton.onClick.AddListener(OnItemClose);
            
            appEventsSystem.OnItemClick += OnItemClick;
            appEventsSystem.OnImageReceived += OnImageReceived;
        }

        private void OnImageReceived(Texture2D texture, string id)
        {
            if (id.Equals(curImageId))
            {
                Rect rect = new Rect(0, 0, texture.width, texture.height);
                Sprite spriteToUse = Sprite.Create(texture,rect, new Vector2(0.5f,0.5f),100);
                uiProgramInfo.Image.sprite = spriteToUse;
                uiProgramInfo.Image.rectTransform.sizeDelta = new Vector2(texture.width, texture.height);
                
                uiProgramInfo.Image.gameObject.SetActive(true);
                uiProgramInfo.Loader.SetActive(false);
            }
        }

        private void OnItemClick(ProgramData programData)
        {
            curImageId = programData.imageData.Id;
            uiProgramInfo.Loader.SetActive(true);
            uiProgramInfo.Image.gameObject.SetActive(false);
            
            uiProgramInfo.Title.text = programData.Title;
            uiProgramInfo.Description.text = programData.Description ?? "Description is not provided";
            uiProgramInfo.Country.text = programData.Country ?? "Country is not provided";
            uiProgramInfo.ContentType.text = programData.ContentType ??  "Type is not provided";
            uiProgramInfo.Creator.text = programData.Creator ?? "Creator is not provided";
        }

        private void OnItemClose()
        {
            uiProgramInfo.Image.sprite = null;
            appEventsSystem.OnItemClose?.Invoke();
        }
    }
}