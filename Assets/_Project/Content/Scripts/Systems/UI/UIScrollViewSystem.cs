using lab.framework;
using UnityEngine;

namespace ao
{
    public class UIScrollViewSystem : BaseInitializableSystem, ITickable
    {
        [Inject] private AppEventsSystem appEventsSystem;

        private UIContentHolder uiContentHolder;
        private UIScrollView uiScrollView;

        private bool groupReceived = false;
        private float updTime;
        
        public override void Initialize(params object[] arg)
        {
            base.Initialize(arg);

            uiContentHolder = GameObject.FindObjectOfType<UIContentHolder>();
            uiScrollView = GameObject.FindObjectOfType<UIScrollView>();

            if (!uiContentHolder || !uiScrollView)
                return;
            
            appEventsSystem.OnNewRequest += OnNewRequest;
            appEventsSystem.OnProgramDataReceived += OnProgramDataReceived;
        }

        public void Tick()
        {            
            if (Mathf.Abs(uiScrollView.ScrollRect.verticalNormalizedPosition) < 0.1f && groupReceived && Time.time - updTime > 0.01f)
            {
                appEventsSystem.OnNextRequest?.Invoke();
                groupReceived = false;
            }
        }
        
        private void OnNewRequest(string query)
        {
            foreach (Transform item in uiContentHolder.transform)
            {
                GameObject.Destroy(item.gameObject);
            }
        }

        private void OnProgramDataReceived(ProgramData programData, bool lastInGroup)
        {
            var item = GameObject.Instantiate(InitSettings.UISettings.ItemPrefab, uiContentHolder.transform, false);
            item.Title.text = programData.Title;
            item.ProgramData = programData;

            item.OnItemClick += appEventsSystem.OnItemClick;

            groupReceived = lastInGroup;
            updTime = Time.time;
        }
    }
}