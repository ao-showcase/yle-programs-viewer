using lab.framework;
using UnityEngine;
using UnityEngine.Events;

namespace ao
{
    public class AppEventsSystem : IDisposable
    {
        public UnityAction<ProgramData, bool> OnProgramDataReceived; // program item data; is item last in data group
        public UnityAction OnNextRequest;
        public UnityAction<string> OnNewRequest; // search request
        public UnityAction<ProgramData> OnItemClick;
        public UnityAction OnItemClose;
        public UnityAction<Texture2D, string> OnImageReceived; // texture; id
        
        public void Dispose()
        {
            OnProgramDataReceived = null;
            OnNextRequest = null;
            OnNewRequest = null;
            OnItemClick = null;
            OnItemClose = null;
            OnImageReceived = null;
        }
    }
}